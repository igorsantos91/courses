package com.alura.igorf.financas.model

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.View
import com.alura.igorf.financas.R
import com.alura.igorf.financas.extension.formatar
import kotlinx.android.synthetic.main.resumo_card.view.*
import java.math.BigDecimal

class ResumoView(context: Context,
                 private val view: View,
                 transacoes: List<Transacao>) {

    private val resumo: Resumo = Resumo(transacoes)
    private val corReceita = ContextCompat.getColor(context, R.color.receita)
    private val corDespesa = ContextCompat.getColor(context, R.color.despesa)

    fun atualizaDados(){
        adicionaReceita()
        adicionaDespesa()
        adicionaTotal()
    }

    private fun adicionaReceita() {
        val totalReceita = resumo.receita()
        with(view.resumo_card_receita){
            setTextColor(corReceita)
            text = totalReceita.formatar()
        }
    }

    private fun adicionaDespesa() {
        val totalDespesa = resumo.despesa()
        with(view.resumo_card_despesa){
            setTextColor(corDespesa)
            text = totalDespesa.formatar()
        }
    }

    private fun adicionaTotal() {
        val total = resumo.total()
        val corResultado = if(total < BigDecimal.ZERO) corDespesa else corReceita

        with(view.resumo_card_total){
            setTextColor(corResultado)
            text = total.formatar()
        }
    }

}