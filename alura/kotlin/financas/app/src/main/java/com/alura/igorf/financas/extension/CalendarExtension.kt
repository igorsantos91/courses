package com.alura.igorf.financas.extension

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun Calendar.formataData(): String{
    val formater = SimpleDateFormat("dd/MM/yyyy")
    return formater.format(this.time)
}