package com.alura.igorf.financas.model

import java.math.BigDecimal

class Resumo(private val transacoes: List<Transacao>) {

    fun receita() = somaPor(Tipo.RECEITA)

    fun despesa() = somaPor(Tipo.DESPESAS)

    fun total() = receita() - despesa()

    private fun somaPor(tipo: Tipo): BigDecimal{
        return transacoes.filter { it.tipo == tipo }
                .sumByDouble { it.valor.toDouble() }
                .toBigDecimal()
    }
}