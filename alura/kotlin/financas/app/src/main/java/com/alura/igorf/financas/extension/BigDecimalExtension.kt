package com.alura.igorf.financas.extension

import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.*

fun BigDecimal.formatar(): String {
    val numberFormat = DecimalFormat.getCurrencyInstance(Locale("pt", "br"))
    return numberFormat.format(this)
}