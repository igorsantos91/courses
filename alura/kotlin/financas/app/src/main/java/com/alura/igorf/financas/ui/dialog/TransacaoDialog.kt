package com.alura.igorf.financas.ui.dialog

import android.app.DatePickerDialog
import android.content.Context
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import com.alura.igorf.financas.R
import com.alura.igorf.financas.extension.formataData
import com.alura.igorf.financas.model.Tipo
import com.alura.igorf.financas.model.Transacao
import kotlinx.android.synthetic.main.form_transacao.view.*
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

abstract class TransacaoDialog(val context: Context, val viewGroup: ViewGroup) {

    protected val viewCriada = criaLayout()
    abstract val tituloBotao: String

    fun configuraDialog(tipo: Tipo, delegate: (transacao: Transacao) -> Unit) {
        configuraCampoCategoria(tipo)
        configuraCampoData()
        configuraFormulario(tipo, delegate)
    }

    private fun configuraCampoCategoria(tipo: Tipo) {
        val categorias = categoriasPor(tipo)

        val adapter = ArrayAdapter.createFromResource(context,
                categorias,
                android.R.layout.simple_spinner_dropdown_item)
        viewCriada.form_transacao_categoria.adapter = adapter
    }

    fun configuraFormulario(tipo: Tipo, delegate: (transacao: Transacao) -> Unit) {
        AlertDialog.Builder(context)
                .setTitle(tituloPor(tipo))
                .setView(viewCriada)
                .setPositiveButton(tituloBotao) { dialogInterface, i ->
                    val valorEmTexto = viewCriada
                            .form_transacao_valor.text.toString()
                    val dataEmTexto = viewCriada
                            .form_transacao_data.text.toString()
                    val categoriaEmTexto = viewCriada
                            .form_transacao_categoria.selectedItem.toString()

                    val valor = try {
                        BigDecimal(valorEmTexto)
                    } catch (exception: NumberFormatException) {
                        Toast.makeText(context,
                                "Falha na conversão de valor",
                                Toast.LENGTH_LONG)
                                .show()
                        BigDecimal.ZERO
                    }

                    val formatoBrasileiro = SimpleDateFormat("dd/MM/yyyy")
                    val dataConvertida: Date = formatoBrasileiro.parse(dataEmTexto)
                    val data = Calendar.getInstance()
                    data.time = dataConvertida

                    val transacaoCriada = Transacao(tipo = tipo,
                            valor = valor,
                            data = data,
                            categoria = categoriaEmTexto)
                    delegate(transacaoCriada)
                }
                .setNegativeButton("Cancelar", null)
                .show()
    }

    private fun configuraCampoData() {
        val hoje = Calendar.getInstance()
        viewCriada.form_transacao_data.setText(Calendar.getInstance().formataData())
        viewCriada.form_transacao_data.setOnClickListener {
            DatePickerDialog(context,
                    DatePickerDialog.OnDateSetListener { view, ano, mes, dia ->
                        val dataSelecionada = Calendar.getInstance()
                        dataSelecionada.set(ano, mes, dia)
                        viewCriada.form_transacao_data
                                .setText(dataSelecionada.formataData())
                    }
                    , hoje.get(Calendar.YEAR), hoje.get(Calendar.MONTH), hoje.get(Calendar.DAY_OF_MONTH))
                    .show()
        }
    }

    private fun criaLayout(): View {
        return LayoutInflater.from(context)
                .inflate(R.layout.form_transacao,
                        viewGroup,
                        false)
    }

    abstract fun tituloPor(tipo: Tipo): Int

    protected fun categoriasPor(tipo: Tipo): Int {
        if (tipo == Tipo.RECEITA) {
            return R.array.categorias_de_receita
        }
        return R.array.categorias_de_despesa
    }
}