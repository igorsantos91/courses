package com.alura.igorf.financas.ui.dialog

import android.content.Context
import android.view.ViewGroup
import com.alura.igorf.financas.R
import com.alura.igorf.financas.model.Tipo

class AdicionaTransacaoDialog(
        viewGroup: ViewGroup,
        context: Context) : TransacaoDialog(context, viewGroup) {

    override val tituloBotao: String
        get() = "Adicionar"

    override fun tituloPor(tipo: Tipo): Int {
        if (tipo == Tipo.RECEITA) {
            return R.string.adiciona_receita
        }
        return R.string.adiciona_despesa
    }

}
