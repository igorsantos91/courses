package com.alura.igorf.financas.ui.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import com.alura.igorf.financas.R
import com.alura.igorf.financas.dao.TransacaoDAO
import com.alura.igorf.financas.model.ResumoView
import com.alura.igorf.financas.model.Tipo
import com.alura.igorf.financas.model.Transacao
import com.alura.igorf.financas.ui.adapter.ListaTransacoesAdapter
import com.alura.igorf.financas.ui.dialog.AdicionaTransacaoDialog
import com.alura.igorf.financas.ui.dialog.AlteraTransacaoDialog
import kotlinx.android.synthetic.main.activity_lista_transacoes.*

class ListaTransacoesActivity: AppCompatActivity() {
    private val dao = TransacaoDAO()
    private val transacoes = dao.transacoes

    companion object {
        private const val  MENU_EXCLUIR = 1
    }

    @Suppress("MoveLambdaOutsideParentheses")
    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_transacoes)

        configuraResumo()
        configuraLista()

        lista_transacoes_adiciona_receita.setOnClickListener {
            configuraDialog(Tipo.RECEITA)
        }

        lista_transacoes_adiciona_despesa.setOnClickListener {
            configuraDialog(Tipo.DESPESAS)
        }
    }

    private fun configuraDialog(tipo: Tipo) {
        AdicionaTransacaoDialog(window.decorView as ViewGroup, this)
            .configuraDialog(tipo) { transacaoAdicionada ->
                dao.adiciona(transacaoAdicionada)
                atualizaTransacoes()
                lista_transacoes_adiciona_menu.close(true)
            }
    }

    private fun atualizaTransacoes() {
        configuraLista()
        configuraResumo()
    }

    private fun configuraResumo() {
        val view: View = window.decorView
        val resumoView = ResumoView(this, view, transacoes)
        resumoView.atualizaDados()
    }

    private fun configuraLista() {
        val transacoesAdapter = ListaTransacoesAdapter(transacoes, this)
        with(lista_transacoes_listview) {
            adapter = transacoesAdapter
            setOnItemClickListener { _, _, position, _ ->
                val transacao = transacoes[position]
                chamaDialogDeAlteracao(transacao, position)
            }

            setOnCreateContextMenuListener  { menu, v, menuInfo ->
                menu.add(Menu.NONE, MENU_EXCLUIR, Menu.NONE, "Remover")
            }
        }
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId == MENU_EXCLUIR) {
            val adapterMenuInfo = item.menuInfo as AdapterView.AdapterContextMenuInfo
            dao.remove(adapterMenuInfo.position)
            atualizaTransacoes()
        }

        return super.onContextItemSelected(item)
    }

    private fun chamaDialogDeAlteracao(transacao: Transacao, position: Int) {
        AlteraTransacaoDialog(window.decorView as ViewGroup, this)
            .configuraDialog(transacao) { transacaoAtualizada ->
                dao.altera(transacaoAtualizada, position)
                atualizaTransacoes()
                lista_transacoes_adiciona_menu.close(true)
            }
    }
}