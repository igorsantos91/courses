package com.alura.igorf.financas.model

enum class Tipo {
    RECEITA, DESPESAS
}