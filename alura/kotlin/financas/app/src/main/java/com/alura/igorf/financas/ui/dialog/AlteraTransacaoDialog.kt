package com.alura.igorf.financas.ui.dialog

import android.content.Context
import android.view.ViewGroup
import com.alura.igorf.financas.R
import com.alura.igorf.financas.extension.formataData
import com.alura.igorf.financas.model.Tipo
import com.alura.igorf.financas.model.Transacao
import kotlinx.android.synthetic.main.form_transacao.view.*

class AlteraTransacaoDialog(
        viewGroup: ViewGroup,
        context: Context): TransacaoDialog(context, viewGroup) {

    override val tituloBotao: String
        get() = "Alterar"

    fun configuraDialog(transacao: Transacao, delegate: (transacao: Transacao) -> Unit) {
        super.configuraDialog(transacao.tipo, delegate)

        inicializaCampos(transacao)
    }

    private fun inicializaCampos(transacao: Transacao) {
        inicializaValor(transacao)
        inicializaData(transacao)
        inicializaCategorias(transacao)
    }

    private fun inicializaData(transacao: Transacao) {
        viewCriada.form_transacao_data.setText(transacao.data.formataData())
    }

    private fun inicializaValor(transacao: Transacao) {
        viewCriada.form_transacao_valor.setText(transacao.valor.toString())
    }

    private fun inicializaCategorias(transacao: Transacao) {
        val categoriasRetornadas = context.resources.getStringArray(categoriasPor(transacao.tipo))
        val posicaoCategoria = categoriasRetornadas.indexOf(transacao.categoria)
        viewCriada.form_transacao_categoria.setSelection(posicaoCategoria, true)
    }

    override fun tituloPor(tipo: Tipo): Int {
        return if (tipo == Tipo.RECEITA)
            R.string.altera_receita
        else
            R.string.altera_despesa
    }
}
