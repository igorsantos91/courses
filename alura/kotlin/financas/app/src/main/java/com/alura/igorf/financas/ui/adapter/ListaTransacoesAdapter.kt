package com.alura.igorf.financas.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.alura.igorf.financas.R
import com.alura.igorf.financas.R.layout.transacao_item
import com.alura.igorf.financas.extension.formataData
import com.alura.igorf.financas.extension.formatar
import com.alura.igorf.financas.model.Tipo
import com.alura.igorf.financas.model.Transacao
import kotlinx.android.synthetic.main.transacao_item.view.*
import java.text.SimpleDateFormat

class ListaTransacoesAdapter(val transacoes: List<Transacao>,
                             val context: Context): BaseAdapter() {

    @SuppressLint("ViewHolder", "SimpleDateFormat", "SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = from(context).inflate(
                transacao_item,
                parent,
                false)

        val item = transacoes[position]

        if( item.tipo == Tipo.RECEITA ){
            view.transacao_valor.setTextColor( ContextCompat.getColor(context, R.color.receita))
            view.transacao_icone.setBackgroundResource(R.drawable.icone_transacao_item_receita)
        }else {
            view.transacao_valor.setTextColor( ContextCompat.getColor(context, R.color.despesa))
            view.transacao_icone.setBackgroundResource(R.drawable.icone_transacao_item_despesa)
        }

        view.transacao_valor.text = item.valor.formatar()
        view.transacao_categoria.text = item.categoria
        view.transacao_data.text = item.data.formataData()

        return view
    }

    override fun getItem(position: Int): Transacao {
        return transacoes[position]
    }

    override fun getItemId(position: Int): Long {
        return 0L
    }

    override fun getCount(): Int {
        return transacoes.size
    }
}